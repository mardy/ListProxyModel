#include "listproxymodel.h"

#include <QCoreApplication>
#include <QQmlEngine>

using namespace lpm;

namespace lpm {

class ListProxyModelPrivate: public QObject {
    Q_OBJECT
    Q_DECLARE_PUBLIC(ListProxyModel)

public:
    ListProxyModelPrivate(ListProxyModel *q);

    void queueUpdate();
    Q_INVOKABLE void update();

    QModelIndex toSource(const QModelIndex &index) const;
    QModelIndex fromSource(const QModelIndex &index, bool &isMapped) const;

    void onRowsInserted(const QModelIndex &parent, int first, int last);
    void onRowsRemoved(const QModelIndex &parent, int first, int last);
    void onColumnsInserted(const QModelIndex &parent, int first, int last);
    void onColumnsRemoved(const QModelIndex &parent, int first, int last);
    void onDataChanged(const QModelIndex &topLeft,
                       const QModelIndex &bottomRight,
                       const QVector<int> &roles);

private:
    QAbstractItemModel *m_model;
    int m_filterRow;
    int m_filterColumn;
    bool m_updateQueued;
    ListProxyModel *q_ptr;
};

} // namespace

ListProxyModelPrivate::ListProxyModelPrivate(ListProxyModel *q):
    m_model(0),
    m_filterRow(-1),
    m_filterColumn(-1),
    m_updateQueued(false),
    q_ptr(q)
{
}

void ListProxyModelPrivate::queueUpdate()
{
    if (!m_updateQueued) {
        QMetaObject::invokeMethod(this, "update", Qt::QueuedConnection);
        m_updateQueued = true;
    }
}

void ListProxyModelPrivate::update()
{
    Q_Q(ListProxyModel);

    m_updateQueued = false;
    q->beginResetModel();
    q->endResetModel();
}

QModelIndex ListProxyModelPrivate::toSource(const QModelIndex &index) const
{
    if (Q_UNLIKELY(!m_model || !index.isValid())) return QModelIndex();

    QModelIndex parent = index.parent();

    if (parent.isValid()) {
        return m_model->index(index.row(), index.column(),
                              toSource(parent));
    }

    int row, column;
    if (m_filterColumn >= 0) {
        column = m_filterColumn;
        row = index.row();
    } else if (m_filterRow >= 0) {
        column = index.row();
        row = m_filterRow;
    } else {
        return QModelIndex();
    }
    return m_model->index(row, column);
}

QModelIndex ListProxyModelPrivate::fromSource(const QModelIndex &index,
                                              bool &isMapped) const
{
    Q_Q(const ListProxyModel);

    if (Q_UNLIKELY(!m_model)) {
        isMapped = false;
        return QModelIndex();
    }

    if (!index.isValid()) {
        isMapped = true;
        return QModelIndex();
    }

    QModelIndex parent = index.parent();

    if (parent.isValid()) {
        QModelIndex mappedParent = fromSource(parent, isMapped);
        return isMapped ?
            q->index(index.row(), index.column(), mappedParent) :
            QModelIndex();
    }

    isMapped = false;
    int row;
    if (m_filterRow >= 0) {
        isMapped = (index.row() == m_filterRow);
        row = index.column();
    } else if (m_filterColumn >= 0) {
        isMapped = (index.column() == m_filterColumn);
        row = index.row();
    }

    return isMapped ? q->index(row, 0) : QModelIndex();
}

void ListProxyModelPrivate::onRowsInserted(const QModelIndex &parent, int first, int last)
{
    Q_Q(ListProxyModel);

    if (parent.isValid()) {
        bool isMapped = false;
        QModelIndex mappedParent = fromSource(parent, isMapped);
        if (!isMapped) return;
        q->beginInsertRows(mappedParent, first, last);
        q->endInsertRows();
    } else {
        if (m_filterRow >= 0) {
            if (m_filterRow >= first) {
                m_filterRow += (last - first + 1);
                Q_EMIT q->filterRowChanged();
            }
        } else if (m_filterColumn >= 0) {
            q->beginInsertRows(QModelIndex(), first, last);
            q->endInsertRows();
        }
    }
}

void ListProxyModelPrivate::onRowsRemoved(const QModelIndex &parent, int first, int last)
{
    Q_Q(ListProxyModel);

    if (parent.isValid()) {
        bool isMapped = false;
        QModelIndex mappedParent = fromSource(parent, isMapped);
        if (!isMapped) return;
        q->beginRemoveRows(mappedParent, first, last);
        q->endRemoveRows();
    } else {
        if (m_filterRow >= 0) {
            if (m_filterRow > last) {
                m_filterRow -= (last - first + 1);
                Q_EMIT q->filterRowChanged();
            } else if (m_filterRow >= first) {
                m_filterRow = -1;
                Q_EMIT q->filterRowChanged();
                queueUpdate();
            }
        } else if (m_filterColumn >= 0) {
            q->beginRemoveRows(QModelIndex(), first, last);
            q->endRemoveRows();
        }
    }
}

void ListProxyModelPrivate::onColumnsInserted(const QModelIndex &parent, int first, int last)
{
    Q_Q(ListProxyModel);

    if (parent.isValid()) {
        bool isMapped = false;
        QModelIndex mappedParent = fromSource(parent, isMapped);
        if (!isMapped) return;
        q->beginInsertColumns(mappedParent, first, last);
        q->endInsertColumns();
    } else {
        if (m_filterRow >= 0) {
            q->beginInsertRows(QModelIndex(), first, last);
            q->endInsertRows();
        } else if (m_filterColumn >= 0) {
            if (m_filterColumn >= first) {
                m_filterColumn += (last - first + 1);
                Q_EMIT q->filterColumnChanged();
            }
        }
    }
}

void ListProxyModelPrivate::onColumnsRemoved(const QModelIndex &parent, int first, int last)
{
    Q_Q(ListProxyModel);

    if (parent.isValid()) {
        bool isMapped = false;
        QModelIndex mappedParent = fromSource(parent, isMapped);
        if (!isMapped) return;
        q->beginRemoveColumns(mappedParent, first, last);
        q->endRemoveRows();
    } else {
        if (m_filterRow >= 0) {
            q->beginRemoveRows(QModelIndex(), first, last);
            q->endRemoveRows();
        } else if (m_filterColumn >= 0) {
            if (m_filterColumn > last) {
                m_filterColumn -= (last - first + 1);
                Q_EMIT q->filterColumnChanged();
            } else if (m_filterColumn >= first) {
                m_filterColumn = -1;
                Q_EMIT q->filterColumnChanged();
                queueUpdate();
            }
        }
    }
}

void ListProxyModelPrivate::onDataChanged(const QModelIndex &topLeft,
                                          const QModelIndex &bottomRight,
                                          const QVector<int> &roles)
{
    Q_Q(ListProxyModel);

    if (topLeft.parent().isValid()) {
        bool isMapped = false;
        QModelIndex mappedTopLeft = fromSource(topLeft, isMapped);
        if (!isMapped) return;
        QModelIndex mappedBottomRight = fromSource(bottomRight, isMapped);
        Q_EMIT q->dataChanged(mappedTopLeft, mappedBottomRight, roles);
    } else {
        if (m_filterRow >= 0) {
            if (m_filterRow >= topLeft.row() &&
                m_filterRow <= bottomRight.row()) {
                Q_EMIT q->dataChanged(q->index(topLeft.column(), 0),
                                      q->index(bottomRight.column(), 0),
                                      roles);
            }
        } else if (m_filterColumn >= 0) {
            if (m_filterColumn >= topLeft.column() &&
                m_filterColumn <= bottomRight.column()) {
                Q_EMIT q->dataChanged(q->index(topLeft.row(), 0),
                                      q->index(bottomRight.row(), 0),
                                      roles);
            }
        }
    }
}

ListProxyModel::ListProxyModel(QObject *parent):
    QAbstractListModel(parent),
    d_ptr(new ListProxyModelPrivate(this))
{
    connect(this, &QAbstractItemModel::rowsInserted, this, &ListProxyModel::countChanged);
    connect(this, &QAbstractItemModel::rowsRemoved, this, &ListProxyModel::countChanged);
    connect(this, &QAbstractItemModel::modelReset, this, &ListProxyModel::countChanged);
}

ListProxyModel::~ListProxyModel()
{
}

void ListProxyModel::setSourceModel(QAbstractItemModel *model)
{
    Q_D(ListProxyModel);

    if (model == d->m_model) return;

    if (d->m_model) {
        d->m_model->disconnect(this);
        d->m_model->disconnect(d);
    }

    if (model) {
        QObject::connect(model, &QAbstractItemModel::modelReset,
                         this, &QAbstractItemModel::modelReset);
        QObject::connect(model, &QAbstractItemModel::rowsInserted,
                         d, &ListProxyModelPrivate::onRowsInserted);
        QObject::connect(model, &QAbstractItemModel::rowsRemoved,
                         d, &ListProxyModelPrivate::onRowsRemoved);
        QObject::connect(model, &QAbstractItemModel::columnsInserted,
                         d, &ListProxyModelPrivate::onColumnsInserted);
        QObject::connect(model, &QAbstractItemModel::columnsRemoved,
                         d, &ListProxyModelPrivate::onColumnsRemoved);
        QObject::connect(model, &QAbstractItemModel::dataChanged,
                         d, &ListProxyModelPrivate::onDataChanged);
    }

    d->m_model = model;
    d->queueUpdate();
    Q_EMIT sourceModelChanged();
}

QAbstractItemModel *ListProxyModel::sourceModel() const
{
    Q_D(const ListProxyModel);
    return d->m_model;
}

void ListProxyModel::setFilterRow(int row)
{
    Q_D(ListProxyModel);
    if (d->m_filterRow == row) return;
    d->m_filterRow = row;
    d->queueUpdate();
    Q_EMIT filterRowChanged();
}

int ListProxyModel::filterRow() const
{
    Q_D(const ListProxyModel);
    return d->m_filterRow;
}

void ListProxyModel::setFilterColumn(int column)
{
    Q_D(ListProxyModel);
    if (d->m_filterColumn == column) return;
    d->m_filterColumn = column;
    d->queueUpdate();
    Q_EMIT filterColumnChanged();
}

int ListProxyModel::filterColumn() const
{
    Q_D(const ListProxyModel);
    return d->m_filterColumn;
}

void ListProxyModel::classBegin()
{
    Q_D(ListProxyModel);
    d->m_updateQueued = true;
}

void ListProxyModel::componentComplete()
{
    Q_D(ListProxyModel);
    d->update();
}

QVariantMap ListProxyModel::get(int row) const
{
    QVariantMap map;
    QModelIndex modelIndex = index(row, 0);
    QHash<int, QByteArray> roles = roleNames();
    for (auto i = roles.begin(); i != roles.end(); i++)
        map.insert(i.value(), data(modelIndex, i.key()));
    return map;
}

QVariant ListProxyModel::get(int row, const QString& roleName) const
{
    int role = roleNames().key(roleName.toLatin1(), -1);
    return data(index(row, 0), role);
}


QHash<int, QByteArray> ListProxyModel::roleNames() const
{
    Q_D(const ListProxyModel);
    return d->m_model ?
        d->m_model->roleNames() : QAbstractItemModel::roleNames();
}

int ListProxyModel::rowCount(const QModelIndex &parent) const
{
    Q_D(const ListProxyModel);

    if (Q_LIKELY(d->m_model)) {
        QModelIndex index = d->toSource(parent);
        if (d->m_filterColumn >= 0) {
            return d->m_model->rowCount(index);
        } else if (d->m_filterRow >= 0) {
            return d->m_model->columnCount(index);
        }
    }
    return 0;
}

bool ListProxyModel::setData(const QModelIndex &index, const QVariant &value,
                             int role)
{
    Q_D(ListProxyModel);

    QModelIndex srcIndex = d->toSource(index);
    if (Q_UNLIKELY(!srcIndex.isValid())) return false;

    return d->m_model->setData(srcIndex, value, role);
}

QVariant ListProxyModel::data(const QModelIndex &index, int role) const
{
    Q_D(const ListProxyModel);

    QModelIndex srcIndex = d->toSource(index);
    if (Q_UNLIKELY(!srcIndex.isValid())) return QVariant();

    return srcIndex.data(role);
}

static void registerListProxyModelTypes() {
    qmlRegisterType<ListProxyModel>("ListProxyModel", 0, 1, "ListProxyModel");
}

Q_COREAPP_STARTUP_FUNCTION(registerListProxyModelTypes)

#include "listproxymodel.moc"
