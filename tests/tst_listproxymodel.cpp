#include "listproxymodel.h"
#include <QSignalSpy>
#include <QStandardItemModel>
#include <QStandardItem>
#include <QTest>

using namespace lpm;

class ListProxyModelTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testRoles();
    void testConstant_data();
    void testConstant();
    void testDataChanged_data();
    void testDataChanged();
    void testRowAdded_data();
    void testRowAdded();
    void testSetData_data();
    void testSetData();
};

void ListProxyModelTest::testRoles()
{
    QStandardItemModel model(1, 1);
    const QHash<int, QByteArray> roleNames {
        { Qt::UserRole + 1, "title" },
        { Qt::UserRole + 2, "author" },
        { Qt::UserRole + 3, "year" },
    };
    model.setItemRoleNames(roleNames);

    ListProxyModel proxy;
    proxy.setSourceModel(&model);

    QCOMPARE(proxy.roleNames(), roleNames);
}

void ListProxyModelTest::testConstant_data()
{
    QTest::addColumn<int>("filterRow");
    QTest::addColumn<int>("filterColumn");
    QTest::addColumn<QList<int>>("expectedData");

    QTest::newRow("first column") <<
        -1 << 0 << QList<int>{ 0, 10, 20, 30 };

    QTest::newRow("first row") <<
        0 << -1 << QList<int>{ 0, 1, 2, 3, 4 };

    QTest::newRow("last row") <<
        3 << -1 << QList<int>{ 30, 31, 32, 33, 34 };
}

void ListProxyModelTest::testConstant()
{
    QFETCH(int, filterRow);
    QFETCH(int, filterColumn);
    QFETCH(QList<int>, expectedData);

    QStandardItemModel model(4, 5);
    int role = Qt::UserRole + 1;

    for (int row = 0; row < 4; row++) {
        for (int col = 0; col < 5; col++) {
            QStandardItem *item = new QStandardItem();
            item->setData(row * 10 + col, role);
            model.setItem(row, col, item);
        }
    }

    ListProxyModel proxy;
    proxy.setSourceModel(&model);
    proxy.setFilterRow(filterRow);
    proxy.setFilterColumn(filterColumn);
    QSignalSpy modelReset(&proxy, &QAbstractItemModel::modelReset);

    modelReset.wait();

    QList<int> data;
    for (int i = 0; i < proxy.rowCount(); i++) {
        data.append(proxy.data(proxy.index(i), role).toInt());
    }

    QCOMPARE(data, expectedData);
}

void ListProxyModelTest::testDataChanged_data()
{
    QTest::addColumn<int>("filterRow");
    QTest::addColumn<int>("filterColumn");
    QTest::addColumn<int>("changedRow");
    QTest::addColumn<int>("changedColumn");
    QTest::addColumn<int>("expectedRow");

    QTest::newRow("uninteresting") <<
        2 << -1 << 1 << 1 << -1;

    QTest::newRow("in row") <<
        0 << -1 << 0 << 2 << 2;

    QTest::newRow("in column") <<
        -1 << 1 << 3 << 1 << 3;
}

void ListProxyModelTest::testDataChanged()
{
    QFETCH(int, filterRow);
    QFETCH(int, filterColumn);
    QFETCH(int, changedRow);
    QFETCH(int, changedColumn);
    QFETCH(int, expectedRow);

    QStandardItemModel model(4, 5);
    int role = Qt::UserRole + 1;

    for (int row = 0; row < 4; row++) {
        for (int col = 0; col < 5; col++) {
            QStandardItem *item = new QStandardItem();
            item->setData(row * 10 + col, role);
            model.setItem(row, col, item);
        }
    }

    ListProxyModel proxy;
    proxy.setSourceModel(&model);
    proxy.setFilterRow(filterRow);
    proxy.setFilterColumn(filterColumn);
    QSignalSpy dataChanged(&proxy, &QAbstractItemModel::dataChanged);

    QModelIndex index = model.index(changedRow, changedColumn);
    model.setData(index, 100);

    if (expectedRow >= 0) {
        QCOMPARE(dataChanged.count(), 1);
        QModelIndex topLeft = dataChanged.at(0).at(0).value<QModelIndex>();
        QModelIndex bottomRight = dataChanged.at(0).at(1).value<QModelIndex>();
        QCOMPARE(topLeft, bottomRight);
        QCOMPARE(topLeft.column(), 0);
        QCOMPARE(topLeft.row(), expectedRow);
    } else {
        QCOMPARE(dataChanged.count(), 0);
    }
}

void ListProxyModelTest::testRowAdded_data()
{
    QTest::addColumn<int>("filterRow");
    QTest::addColumn<int>("filterColumn");
    QTest::addColumn<QString>("changeText");
    /*
     * 0 = no change
     * <0 = row removed: start counting from 1
     * >0 = row added: start counting from 1
     */
    QTest::addColumn<int>("expectedRowChange");

    QTest::newRow("uninteresting") <<
        2 << -1 << "add row 3" << 0;

    QTest::newRow("uninteresting, row moved") <<
        2 << -1 << "add row 2" << 0;

    QTest::newRow("add by row") <<
        2 << -1 << "add column 2" << 3;

    QTest::newRow("remove by row") <<
        0 << -1 << "remove column 1" << -2;

    QTest::newRow("uninteresting by col") <<
        -1 << 1 << "remove column 0" << 0;

    QTest::newRow("add by column") <<
        -1 << 1 << "add row 1" << 2;

    QTest::newRow("remove by column") <<
        -1 << 3 << "remove row 2" << -3;
}

void ListProxyModelTest::testRowAdded()
{
    QFETCH(int, filterRow);
    QFETCH(int, filterColumn);
    QFETCH(QString, changeText);
    QFETCH(int, expectedRowChange);

    QStandardItemModel model(4, 5);
    int role = Qt::UserRole + 1;

    for (int row = 0; row < 4; row++) {
        for (int col = 0; col < 5; col++) {
            QStandardItem *item = new QStandardItem();
            item->setData(row * 10 + col, role);
            model.setItem(row, col, item);
        }
    }

    ListProxyModel proxy;
    proxy.setSourceModel(&model);
    proxy.setFilterRow(filterRow);
    proxy.setFilterColumn(filterColumn);
    QSignalSpy rowsInserted(&proxy, &QAbstractItemModel::rowsInserted);
    QSignalSpy rowsRemoved(&proxy, &QAbstractItemModel::rowsRemoved);

    QStringList words = changeText.split(' ');
    QCOMPARE(words.count(), 3);
    int number = words[2].toInt();
    QModelIndex index;
    if (words[0] == "add") {
        if (words[1] == "row") {
            model.insertRow(number);
        } else {
            model.insertColumn(number);
        }
    } else {
        if (words[1] == "row") {
            model.removeRows(number, 1);
        } else {
            model.removeColumns(number, 1);
        }
    }

    if (expectedRowChange > 0) {
        QCOMPARE(rowsInserted.count(), 1);
        QCOMPARE(rowsRemoved.count(), 0);
        QVERIFY(!rowsInserted.at(0).at(0).value<QModelIndex>().isValid());
        int row = rowsInserted.at(0).at(1).toInt();
        QCOMPARE(rowsInserted.at(0).at(2).toInt(), row);
        QCOMPARE(row, expectedRowChange - 1);
    } else if (expectedRowChange < 0) {
        QCOMPARE(rowsInserted.count(), 0);
        QCOMPARE(rowsRemoved.count(), 1);
        QVERIFY(!rowsRemoved.at(0).at(0).value<QModelIndex>().isValid());
        int row = rowsRemoved.at(0).at(1).toInt();
        QCOMPARE(rowsRemoved.at(0).at(2).toInt(), row);
        QCOMPARE(row, -expectedRowChange - 1);
    } else {
        QCOMPARE(rowsInserted.count(), 0);
        QCOMPARE(rowsRemoved.count(), 0);
    }
}

void ListProxyModelTest::testSetData_data()
{
    QTest::addColumn<int>("filterRow");
    QTest::addColumn<int>("filterColumn");
    QTest::addColumn<int>("changedRow");
    QTest::addColumn<QVariant>("value");
    QTest::addColumn<int>("expectedSourceRow");
    QTest::addColumn<int>("expectedSourceColumn");

    QTest::newRow("row, first") <<
        2 << -1 << 0 << QVariant("Hello") << 2 << 0;

    QTest::newRow("row, last") <<
        1 << -1 << 4 << QVariant("Hi") << 1 << 4;

    QTest::newRow("column, middle") <<
        -1 << 2 << 1 << QVariant("How do you do?") << 1 << 2;
}

void ListProxyModelTest::testSetData()
{
    QFETCH(int, filterRow);
    QFETCH(int, filterColumn);
    QFETCH(int, changedRow);
    QFETCH(QVariant, value);
    QFETCH(int, expectedSourceRow);
    QFETCH(int, expectedSourceColumn);

    QStandardItemModel model(4, 5);
    int role = Qt::UserRole + 1;

    for (int row = 0; row < 4; row++) {
        for (int col = 0; col < 5; col++) {
            QStandardItem *item = new QStandardItem();
            item->setData(row * 10 + col, role);
            model.setItem(row, col, item);
        }
    }

    ListProxyModel proxy;
    proxy.setSourceModel(&model);
    proxy.setFilterRow(filterRow);
    proxy.setFilterColumn(filterColumn);
    QSignalSpy dataChanged(&proxy, &QAbstractItemModel::dataChanged);

    QModelIndex index = proxy.index(changedRow);
    proxy.setData(index, value, role);

    /* Check that the signal was emitted with the right parameters */
    QCOMPARE(dataChanged.count(), 1);
    QModelIndex topLeft = dataChanged.at(0).at(0).value<QModelIndex>();
    QModelIndex bottomRight = dataChanged.at(0).at(1).value<QModelIndex>();
    QCOMPARE(topLeft, bottomRight);
    QCOMPARE(topLeft.column(), 0);
    QCOMPARE(topLeft.row(), changedRow);

    /* Check that the source model has been changed */
    index = model.index(expectedSourceRow, expectedSourceColumn);
    QCOMPARE(index.data(role), value);
}

QTEST_GUILESS_MAIN(ListProxyModelTest)
#include "tst_listproxymodel.moc"
