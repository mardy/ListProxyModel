TEMPLATE = app
TARGET = tst_listproxymodel
QT += qml quick
CONFIG += c++11 warn_on qmltestcase qml_debug no_keywords

include(../ListProxyModel.pri)

INCLUDEPATH += ../

SOURCES += \
    tst_listproxymodel.cpp
