ListProxyModel
==============

ListProxyModel turns a tabular QAbstractItemModel into a list model, easily
usable in QML. This is done by selecting a column or a row of the source model,
which will be the only one visible in the ListProxyModel.

ListProxyModel can be used in C++ and in QML projects.

Install
-------
##### With [qpm](https://qpm.io) :
1. `qpm install it.mardy.ListProxyModel`
2. add `include(vendor/vendor.pri)` in your .pro if it is not already done
3. `import ListProxyModel 0.1` to use this library in your QML files

##### Without qpm :
1. clone or download this repository
2. add `include  (<path/to/ListProxyModel>/ListProxyModel.pri)` in your `.pro`
3. `import ListProxyModel 0.1` to use this library in your QML files

Sample Usage
------------

To select the second column of a tabular model:
```qml
import QtQuick 2.2
import QtQuick.Controls 1.2
import ListProxyModel 0.1

ApplicationWindow {
    visible: true
    width: 640
    height: 480

    ListProxyModel {
        id: personProxyModel
        sourceModel: personModel // defined in C++ as a QAbstractItemModel subclass
        filterColumn: 1
    }

    ListView {
        anchors.fill: parent
        model: personProxyModel
        delegate: Text { text: firstName + " " + lastName}
    }
}
```
Similarly, you can use the `filterRow` property to select a row from the table.

License
-------
This library is licensed under the LGPL 3.0 License.

Documentation
-------------
This component is a subclass of
[`QAbstractListModel`](http://doc.qt.io/qt-5/qabstractlistmodel.html). To use
it, you need to set the `sourceModel` property to a
[`QAbstractItemModel*`](http://doc.qt.io/qt-5/qabstractitemmodel.html) and set
either the `filterRow` or `filterColumn` properties.

#### Properties
<li>
__`sourceModel`__ : _`QAbstractItemModel*`_
This property holds the source model of this proxy model.
</li>

<li>
__`filterColumn`__ : _int_
The column number (starting from 0) of the source tabular model to expose.
</li>

<li>
__`filterRow`__ : _int_
The row number (starting from 0) of the source tabular model to expose. Note
that the model will be transposed: the values stored in the columns of the
selected row will be exposed as rows in the filtered model.
</li>


Contributing
------------
Don't hesitate to open an issue about a suggestion, a bug, a lack of clarity in the documentation, etc.

Pull requests are also welcome, if it's a important change you should open an issue first though.
