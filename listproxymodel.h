#ifndef LISTPROXYMODEL_H
#define LISTPROXYMODEL_H

#include <QAbstractListModel>
#include <QQmlParserStatus>
#include <QScopedPointer>

namespace lpm {

class ListProxyModelPrivate;
class ListProxyModel: public QAbstractListModel, public QQmlParserStatus
{
    Q_OBJECT
    Q_INTERFACES(QQmlParserStatus)

    Q_PROPERTY(int count READ rowCount NOTIFY countChanged)

    Q_PROPERTY(QAbstractItemModel *sourceModel READ sourceModel \
               WRITE setSourceModel NOTIFY sourceModelChanged)
    Q_PROPERTY(int filterRow READ filterRow WRITE setFilterRow \
               NOTIFY filterRowChanged)
    Q_PROPERTY(int filterColumn READ filterColumn WRITE setFilterColumn \
               NOTIFY filterColumnChanged)

public:
    ListProxyModel(QObject *parent = 0);
    ~ListProxyModel();

    void setSourceModel(QAbstractItemModel *model);
    QAbstractItemModel *sourceModel() const;

    void setFilterRow(int row);
    int filterRow() const;

    void setFilterColumn(int column);
    int filterColumn() const;

    void classBegin() override;
    void componentComplete() override;

    Q_INVOKABLE QVariantMap get(int row) const;
    Q_INVOKABLE QVariant get(int row, const QString &roleName) const;


    QHash<int, QByteArray> roleNames() const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;
    QVariant data(const QModelIndex &index,
                  int role = Qt::DisplayRole) const override;

Q_SIGNALS:
    void countChanged();
    void sourceModelChanged();
    void filterRowChanged();
    void filterColumnChanged();

private:
    QScopedPointer<ListProxyModelPrivate> d_ptr;
    Q_DECLARE_PRIVATE(ListProxyModel)
};

} // namespace

#endif // LISTPROXYMODEL_H
